package no.tormoder.blackjack.core;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import no.tormoder.blackjack.deck.Card;
import no.tormoder.blackjack.deck.Deck;
import no.tormoder.blackjack.deck.EmptyDeckException;
import no.tormoder.blackjack.deck.Rank;
import no.tormoder.blackjack.deck.Suit;
import no.tormoder.blackjack.player.Player;

class BlackjackTest {
	
	@Test
	@DisplayName("Initial: Both Blackjack")
	void testPlay1() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(
						new Card(Suit.CLUBS, Rank.ACE),
						new Card(Suit.SPADES, Rank.ACE),
						new Card(Suit.CLUBS, Rank.KING),
						new Card(Suit.SPADES, Rank.KING)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(21, player.score());
		assertEquals(21, dealer.score());
		assertEquals(player, winner);
	}
	
	@Test
	@DisplayName("Initial: Player Blackjack")
	void testPlay2() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(
						new Card(Suit.CLUBS, Rank.ACE),
						new Card(Suit.CLUBS, Rank.THREE),
						new Card(Suit.SPADES, Rank.QUEEN),
						new Card(Suit.SPADES, Rank.ACE)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(21, player.score());
		assertEquals(14, dealer.score());
		assertEquals(player, winner);
	}
	
	
	@Test
	@DisplayName("Initial: Dealer Blackjack")
	void testPlay3() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(
						new Card(Suit.CLUBS, Rank.ACE),
						new Card(Suit.SPADES, Rank.ACE),
						new Card(Suit.CLUBS, Rank.THREE),
						new Card(Suit.SPADES, Rank.QUEEN)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(14, player.score());
		assertEquals(21, dealer.score());
		assertEquals(dealer, winner);
	}
	
	@Test
	@DisplayName("Initial: Both AceAce")
	void testPlay4() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(
						new Card(Suit.CLUBS, Rank.ACE),
						new Card(Suit.DIAMONDS, Rank.ACE),
						new Card(Suit.HEARTS, Rank.ACE),
						new Card(Suit.SPADES, Rank.ACE)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(22, player.score());
		assertEquals(22, dealer.score());
		assertEquals(dealer, winner);
	}
	
	@Test
	@DisplayName("Initial: Player AceAce - Dealer Lower")
	void testPlay5() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(
						new Card(Suit.DIAMONDS, Rank.ACE),
						new Card(Suit.CLUBS, Rank.NINE),
						new Card(Suit.SPADES, Rank.ACE),
						new Card(Suit.HEARTS, Rank.NINE)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(22, player.score());
		assertEquals(18, dealer.score());
		assertEquals(dealer, winner);
	}

	@Test
	@DisplayName("Initial: Dealer AceAce - Player Lower")
	void testPlay6() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.NINE),
						new Card(Suit.DIAMONDS, Rank.ACE),
						new Card(Suit.HEARTS, Rank.NINE),
						new Card(Suit.SPADES, Rank.ACE)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(18, player.score());
		assertEquals(22, dealer.score());
		assertEquals(player, winner);
	}
	
	@Test
	@DisplayName("Player 17 from start - dealer draws one card, blackjack")
	void testPlay7() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.EIGHT),
						new Card(Suit.DIAMONDS, Rank.SIX),
						new Card(Suit.HEARTS, Rank.NINE),
						new Card(Suit.DIAMONDS, Rank.FOUR),
						new Card(Suit.SPADES, Rank.ACE)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(17, player.score());
		assertEquals(21, dealer.score());
		assertEquals(dealer, winner);
	}

	@Test
	@DisplayName("Player 17 from start - dealer draws one card, 22")
	void testPlay8() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.EIGHT),
						new Card(Suit.DIAMONDS, Rank.SIX),
						new Card(Suit.HEARTS, Rank.NINE),
						new Card(Suit.DIAMONDS, Rank.FIVE),
						new Card(Suit.SPADES, Rank.ACE)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(17, player.score());
		assertEquals(22, dealer.score());
		assertEquals(player, winner);
	}

	@Test
	@DisplayName("Player 18 from start - dealer draws cards until 18, and then 20")
	void testPlay9() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.EIGHT),
						new Card(Suit.DIAMONDS, Rank.ACE),
						new Card(Suit.HEARTS, Rank.TEN),
						new Card(Suit.CLUBS, Rank.DEUCE),
						new Card(Suit.HEARTS, Rank.DEUCE),
						new Card(Suit.HEARTS, Rank.THREE),
						new Card(Suit.DIAMONDS, Rank.DEUCE)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(18, player.score());
		assertEquals(20, dealer.score());
		assertEquals(dealer, winner);
	}

	@Test
	@DisplayName("Player 18 from start - dealer draws cards until 18, and then 22")
	void testPlay10() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.EIGHT),
						new Card(Suit.DIAMONDS, Rank.ACE),
						new Card(Suit.HEARTS, Rank.TEN),
						new Card(Suit.CLUBS, Rank.DEUCE),
						new Card(Suit.HEARTS, Rank.DEUCE),
						new Card(Suit.HEARTS, Rank.THREE),
						new Card(Suit.DIAMONDS, Rank.FOUR)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(18, player.score());
		assertEquals(22, dealer.score());
		assertEquals(player, winner);
	}

	@Test
	@DisplayName("Player 10 from start, draws 15, 17 - dealer 10, draws 16, 20")
	void testPlay11() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.EIGHT),
						new Card(Suit.DIAMONDS, Rank.FIVE),
						new Card(Suit.CLUBS, Rank.DEUCE),
						new Card(Suit.HEARTS, Rank.FIVE),
						new Card(Suit.CLUBS, Rank.FIVE),
						new Card(Suit.HEARTS, Rank.DEUCE),
						new Card(Suit.HEARTS, Rank.SIX),
						new Card(Suit.HEARTS, Rank.FOUR)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(17, player.score());
		assertEquals(20, dealer.score());
		assertEquals(dealer, winner);
	}

	@Test
	@DisplayName("Player 10 from start, draws 15, 22 - dealer 10")
	void testPlay12() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.EIGHT),
						new Card(Suit.DIAMONDS, Rank.FIVE),
						new Card(Suit.CLUBS, Rank.DEUCE),
						new Card(Suit.HEARTS, Rank.FIVE),
						new Card(Suit.CLUBS, Rank.FIVE),
						new Card(Suit.HEARTS, Rank.SEVEN)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(22, player.score());
		assertEquals(10, dealer.score());
		assertEquals(dealer, winner);
	}

	@Test
	@DisplayName("Game from assignment")
	void testPlay13() throws EmptyDeckException {
		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Deck deck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.ACE),
						new Card(Suit.DIAMONDS, Rank.FIVE),
						new Card(Suit.HEARTS, Rank.NINE),
						new Card(Suit.HEARTS, Rank.QUEEN),
						new Card(Suit.SPADES, Rank.EIGHT)
				)
		));
		Blackjack game = new Blackjack(player, dealer, deck);
		Player winner = game.play();
		assertEquals(20, player.score());
		assertEquals(23, dealer.score());
		assertEquals(player, winner);
	}

	@Test
	@DisplayName("Handle insufficient number of cards in deck")
	void testPlay14() {
		Assertions.assertThrows(EmptyDeckException.class, () -> {
			Player player = new Player("sam");
			Player dealer = new Player("dealer");
			Deck deck = new Deck(new ArrayList<>(
					Arrays.asList(			
						new Card(Suit.CLUBS, Rank.ACE),
						new Card(Suit.DIAMONDS, Rank.FIVE),
						new Card(Suit.HEARTS, Rank.NINE),
						new Card(Suit.HEARTS, Rank.QUEEN)
					)
			));
			Blackjack game = new Blackjack(player, dealer, deck);
			game.play();
		});
	}

}