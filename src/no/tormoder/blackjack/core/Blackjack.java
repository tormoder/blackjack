package no.tormoder.blackjack.core;

import no.tormoder.blackjack.deck.Deck;
import no.tormoder.blackjack.deck.EmptyDeckException;
import no.tormoder.blackjack.player.Player;

public class Blackjack {
	private static final int INITIAL_AA_SCORE = 22;
	private static final int PLAYER_STOP_DRAW_SCORE = 17;
	private static final int MAX_SCORE = 21;

	private final Player dealer;
	private final Player player;
	private final Deck deck;

	public Blackjack(Player player, Player dealer, Deck deck) {
		this.dealer = dealer;
		this.player = player;
		this.deck = deck;
	}

	public Player play() throws EmptyDeckException {
		// Draw two cards each.
		for (int i = 0; i < 2; i++) {
			this.player.addCard(deck.drawCard());
			this.dealer.addCard(deck.drawCard());
		}

		// Initial round: Blackjack.
		if (this.player.hasBlackjack() && this.dealer.hasBlackjack()) {
			return this.player;
		}
		if (this.player.hasBlackjack()) {
			return this.player;
		}
		if (this.dealer.hasBlackjack()) {
			return this.dealer;
		}

		// Initial round: A+A.
		if (this.player.score() == Blackjack.INITIAL_AA_SCORE && this.dealer.score() == Blackjack.INITIAL_AA_SCORE) {
			return this.dealer;
		}
		if (this.player.score() == Blackjack.INITIAL_AA_SCORE) {
			return this.dealer;
		}
		if (this.dealer.score() == Blackjack.INITIAL_AA_SCORE) {
			return this.player;
		}

		// Player.
		while (this.player.score() < Blackjack.PLAYER_STOP_DRAW_SCORE) {
			this.player.addCard(this.deck.drawCard());
			if (this.player.score() > Blackjack.MAX_SCORE) {
				return this.dealer;
			}
		}

		// Dealer.
		while (this.dealer.score() <= this.player.score()) {
			this.dealer.addCard(this.deck.drawCard());
			if (this.dealer.score() > Blackjack.MAX_SCORE) {
				return this.player;
			}
		}

		// Highest score wins.
		if (this.player.score() > this.dealer.score()) {
			return this.player;
		} else {
			return this.dealer;
		}
	}

}