package no.tormoder.blackjack;

import java.io.FileNotFoundException;
import java.io.IOException;

import no.tormoder.blackjack.core.Blackjack;
import no.tormoder.blackjack.deck.Deck;
import no.tormoder.blackjack.deck.EmptyDeckException;
import no.tormoder.blackjack.deck.IllegalCardStringException;
import no.tormoder.blackjack.player.Player;

public class Main {

	public static void main(String[] args) {
		Deck deck = null;

		switch (args.length) {
		case 0:
			deck = new Deck(true);
			break;
		case 1:
			try {
				deck = new Deck(args[0]);
				if (deck.isEmpty()) {
					exit("provided deck was empty", 1, null);
				}
			} catch (FileNotFoundException e) {
				exit("file not found", 1, e);
			} catch (IOException e) {
				exit("i/o error reading file", 1, e);
			} catch (IllegalCardStringException e) {
				exit("illegal card format when parsing cards from file", 1, e);
			}
			break;
		default:
			exit("max one argument please...", 1, null);
		}

		Player player = new Player("sam");
		Player dealer = new Player("dealer");
		Blackjack game = new Blackjack(player, dealer, deck);

		try {
			Player winner = game.play();
			System.out.println(winner.getName());
			System.out.println(player.nameAndCardsToString());
			System.out.println(dealer.nameAndCardsToString());
		} catch (EmptyDeckException e) {
			exit("not enough cards in deck to finish game", 1, null);
		}
	}

	public static void exit(String message, int status, Exception e) {
		System.err.print("error: ");
		System.err.print(message);
		if (e != null) {
			System.err.print(": ");
			System.err.println(e.getMessage());
		}
		System.exit(status);
	}

}