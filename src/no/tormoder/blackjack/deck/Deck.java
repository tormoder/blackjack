package no.tormoder.blackjack.deck;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {

	private List<Card> cards;

	public Deck(boolean shuffle) {
		this.initStandard();
		if (shuffle) {
			this.shuffle();
		}
	}

	// List of cards in top to bottom of deck order.
	public Deck(List<Card> cards) {
		this.cards = cards;
		Collections.reverse(this.cards);
	}

	// List of cards from CSV file.
	// Single line with card values separated by ','.
	// Card value format: {C,D,H,S}{2-10,J,Q,K,A}.
	// Top to bottom of deck order.
	public Deck(String csvFilepath) throws IOException, IllegalCardStringException {
		this.cards = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(csvFilepath))) {
			String cardsLine = br.readLine();
			if (cardsLine == null) {
				return;
			}

			String[] cardStrings = cardsLine.split(",");
			if (cardStrings == null) {
				return;
			}

			for (String cardString : cardStrings) {
				Card card = new Card(cardString.trim());
				cards.add(card);
			}

			Collections.reverse(this.cards);
		}
	}

	public boolean isEmpty() {
		return this.cards.isEmpty();
	}

	public Card drawCard() throws EmptyDeckException {
		if (this.isEmpty()) {
			throw new EmptyDeckException();
		}

		return this.cards.remove(this.cards.size() - 1);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();

		if (this.cards.isEmpty()) {
			sb.append("empty deck");
			return sb.toString();
		}

		for (int i = 0; i < this.cards.size(); i++) {
			if (i != 0) {
				sb.append(",");
			}
			sb.append(this.cards.get(i));
		}

		return sb.toString();
	}

	private void initStandard() {
		this.cards = new ArrayList<>(52);
		for (Suit suit : Suit.values()) {
			for (Rank rank : Rank.values()) {
				this.cards.add(new Card(suit, rank));
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cards == null) ? 0 : cards.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deck other = (Deck) obj;
		return this.cards.equals(other.cards);
	}

	private void shuffle() {
		Collections.shuffle(this.cards);
	}

}