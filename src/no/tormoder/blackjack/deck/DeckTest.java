package no.tormoder.blackjack.deck;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class DeckTest {

	@Test
	@DisplayName("Deck from assignment")
	void testDeckString1() throws IOException, IllegalCardStringException {
		Deck expectedDeck = new Deck(new ArrayList<>(
				Arrays.asList(			
						new Card(Suit.CLUBS, Rank.ACE),
						new Card(Suit.DIAMONDS, Rank.FIVE),
						new Card(Suit.HEARTS, Rank.NINE),
						new Card(Suit.HEARTS, Rank.QUEEN),
						new Card(Suit.SPADES, Rank.EIGHT)
				)
		));

		Deck deck = new Deck("assignment.csv");
		assertEquals(expectedDeck, deck);
	}
	@Test
	@DisplayName("Full")
	void testDeckString2() throws IOException, IllegalCardStringException {
		Deck expectedDeck = new Deck(false);
		Deck deck = new Deck("full.csv");
		assertEquals(expectedDeck, deck);
	}

	@Test
	@DisplayName("Empty deck")
	void testDeckString3() throws IOException, IllegalCardStringException {
		Deck expectedDeck = new Deck(new ArrayList<>());
		Deck deck = new Deck("empty.csv");
		assertEquals(expectedDeck, deck);
	}

	@Test
	@DisplayName("Illegal card string")
	void testDeckString4() throws IOException {
			Assertions.assertThrows(IllegalCardStringException.class, () -> {
			new Deck("illegal.csv");
		});
	}

}
