package no.tormoder.blackjack.deck;

public class IllegalCardStringException extends Exception {

	private static final long serialVersionUID = 1L;

	private final String cardString;

	public IllegalCardStringException(String cardString) {
		super();
		this.cardString = cardString;
	}

	@Override
	public String getMessage() {
		return String.format("illegal card string: %s", this.cardString);
	}

}