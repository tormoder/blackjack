package no.tormoder.blackjack.deck;

public enum Suit {

	CLUBS('C'), DIAMONDS('D'), HEARTS('H'), SPADES('S');

	private final char suitChar;

	Suit(char suitChar) {
		this.suitChar = suitChar;
	}

	char suitChar() {
		return suitChar;
	}

}