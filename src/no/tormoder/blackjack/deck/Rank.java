package no.tormoder.blackjack.deck;

public enum Rank {
	
	DEUCE("2", 2),
	THREE("3",3),
	FOUR("4", 4),
	FIVE("5", 5),
	SIX("6", 6),
	SEVEN("7", 7),
	EIGHT("8", 8),
	NINE("9", 9),
	TEN("10", 10),
	JACK("J", 10),
	QUEEN("Q", 10),
	KING("K", 10),
	ACE("A", 11);
	
	private final String rankString;
	private final int blackjackValue;
	
	Rank(String rankString, int blackjackValue) {
		this.rankString = rankString;
		this.blackjackValue = blackjackValue;
	}
	
	public int blackjackValue() {
		return this.blackjackValue;
	}

	@Override
	public String toString() {
		return this.rankString;
	}

}
