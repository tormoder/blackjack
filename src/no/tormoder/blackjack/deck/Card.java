package no.tormoder.blackjack.deck;

public class Card {

	private final Suit suit;
	private final Rank rank;

	public Card(Suit suit, Rank rank) {
		this.suit = suit;
		this.rank = rank;
	}

	public Suit getSuit() {
		return suit;
	}

	public Rank getRank() {
		return rank;
	}

	public int blackjackValue() {
		return this.rank.blackjackValue();
	}

	public String toString() {
		return String.format("%c%s", this.suit.suitChar(), this.rank);
	}

	public Card(String cardString) throws IllegalCardStringException {
		if (cardString.length() > 3 || cardString.length() < 2) {
			throw new IllegalCardStringException(cardString);
		}

		char suitChar = cardString.charAt(0);
		switch (suitChar) {
		case 'C':
			this.suit = Suit.CLUBS;
			break;
		case 'D':
			this.suit = Suit.DIAMONDS;
			break;
		case 'H':
			this.suit = Suit.HEARTS;
			break;
		case 'S':
			this.suit = Suit.SPADES;
			break;
		default:
			throw new IllegalCardStringException(cardString);
		}

		if (cardString.length() == 3) {
			// Must be 10.
			if (cardString.charAt(1) != '1' || cardString.charAt(2) != '0') {
				throw new IllegalCardStringException(cardString);
			}
			this.rank = Rank.TEN;
		} else {
			// Must be A,2-9,J,Q,K.
			switch (cardString.charAt(1)) {
			case 'A':
				this.rank = Rank.ACE;
				break;
			case '2':
				this.rank = Rank.DEUCE;
				break;
			case '3':
				this.rank = Rank.THREE;
				break;
			case '4':
				this.rank = Rank.FOUR;
				break;
			case '5':
				this.rank = Rank.FIVE;
				break;
			case '6':
				this.rank = Rank.SIX;
				break;
			case '7':
				this.rank = Rank.SEVEN;
				break;
			case '8':
				this.rank = Rank.EIGHT;
				break;
			case '9':
				this.rank = Rank.NINE;
				break;
			case 'J':
				this.rank = Rank.JACK;
				break;
			case 'Q':
				this.rank = Rank.QUEEN;
				break;
			case 'K':
				this.rank = Rank.KING;
				break;
			default:
				throw new IllegalCardStringException(cardString);
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rank == null) ? 0 : rank.hashCode());
		result = prime * result + ((suit == null) ? 0 : suit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (rank != other.rank)
			return false;
		return (suit == other.suit);
	}

}