package no.tormoder.blackjack.player;

import java.util.ArrayList;

import no.tormoder.blackjack.deck.Card;

public class Player {

	private final String name;
	private final ArrayList<Card> cards = new ArrayList<>();

	public Player(String name) {
		this.name = name;
	}

	public int score() {
		int score = 0;
		for (Card card : cards) {
			score += card.blackjackValue();
		}
		return score;
	}

	public boolean hasBlackjack() {
		return this.score() == 21;
	}

	public void addCard(Card card) {
		this.cards.add(card);
	}

	public String getName() {
		return name;
	}

	public String nameAndCardsToString() {
		StringBuilder sb = new StringBuilder(this.getName());
		sb.append(": ");

		if (this.cards.isEmpty()) {
			sb.append("no cards");
			return sb.toString();
		}

		for (int i = 0; i < this.cards.size(); i++) {
			if (i != 0) {
				sb.append(", ");
			}
			sb.append(this.cards.get(i));
		}

		return sb.toString();
	}

	public String toString() {
		return this.getName();
	}

}